package com.aryanto.widi.favoritemovies.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aryanto.widi.favoritemovies.R;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.MovieColumns.MOVIE_OVERVIEW;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.MovieColumns.MOVIE_POSTER;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.MovieColumns.MOVIE_RELEASE;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.MovieColumns.MOVIE_TITLE;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.getColumnString;

public class AdapterFavorite extends CursorAdapter {

    public AdapterFavorite(Context context, Cursor c, boolean autoRequery){
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.favorite_item, parent, false);
        return view;
    }

    @Override
    public Cursor getCursor(){
        return super.getCursor();
    }

    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        if (cursor != null){
            TextView tvTitle = view.findViewById(R.id.tv_item_name);
            TextView tvOverview = view.findViewById(R.id.tv_item_overview);
            TextView tvRelease = view.findViewById(R.id.tv_item_release);
            ImageView poster = view.findViewById(R.id.img_item_photo);
            Button btnShare = view.findViewById(R.id.btn_set_share);

            tvTitle.setText(getColumnString(cursor, MOVIE_TITLE));
            tvOverview.setText(getColumnString(cursor, MOVIE_OVERVIEW));

            String dateRelease = parseDate(getColumnString(cursor, MOVIE_RELEASE));
            tvRelease.setText(dateRelease);

            Glide.with(context).load("http://image.tmdb.org/t/p/w500/"+getColumnString(cursor, MOVIE_POSTER)).into(poster);

            final String title = getColumnString(cursor, MOVIE_TITLE);
            final String overview = getColumnString(cursor, MOVIE_OVERVIEW);
            btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_TEXT, title+" \n"+overview);
                    intent.setType("text/plain");
                    context.startActivity(intent);
                    Toast.makeText(context, "Share "+ title, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private String parseDate(String tgl){
        String inputDate = "yyyy-MM-dd";
        String outputDate = "EEEE, MMM dd, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputDate, Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputDate, Locale.US);
        Date date;
        String string = null;

        try {
            date = inputFormat.parse(tgl);
            string = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return string;
    }
}
