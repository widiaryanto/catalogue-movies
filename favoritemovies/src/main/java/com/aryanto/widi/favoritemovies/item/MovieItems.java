package com.aryanto.widi.favoritemovies.item;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import static android.provider.BaseColumns._ID;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.MovieColumns.MOVIE_OVERVIEW;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.MovieColumns.MOVIE_POSTER;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.MovieColumns.MOVIE_RATE;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.MovieColumns.MOVIE_RATECOUNT;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.MovieColumns.MOVIE_RELEASE;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.MovieColumns.MOVIE_TITLE;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.getColumnInt;
import static com.aryanto.widi.favoritemovies.databases.DatabaseContract.getColumnString;

public class MovieItems implements Parcelable {

    private int id;
    private String movPoster, movTitle, movOverview, movRelease, movRateCount, movRate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMovPoster() {
        return movPoster;
    }

    public void setMovPoster(String movPoster) {
        this.movPoster = movPoster;
    }

    public String getMovTitle() {
        return movTitle;
    }

    public void setMovTitle(String movTitle) {
        this.movTitle = movTitle;
    }

    public String getMovOverview() {
        return movOverview;
    }

    public void setMovOverview(String movOverview) {
        this.movOverview = movOverview;
    }

    public String getMovRelease() {
        return movRelease;
    }

    public void setMovRelease(String movRelease) {
        this.movRelease = movRelease;
    }

    public String getMovRateCount() {
        return movRateCount;
    }

    public void setMovRateCount(String movRateCount) {
        this.movRateCount = movRateCount;
    }

    public String getMovRate() {
        return movRate;
    }

    public void setMovRate(String movRate) {
        this.movRate = movRate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.movPoster);
        dest.writeString(this.movTitle);
        dest.writeString(this.movOverview);
        dest.writeString(this.movRelease);
        dest.writeString(this.movRateCount);
        dest.writeString(this.movRate);
    }

    public MovieItems(Cursor cursor){
        this.id = getColumnInt(cursor, _ID);
        this.movTitle = getColumnString(cursor, MOVIE_TITLE);
        this.movOverview = getColumnString(cursor, MOVIE_OVERVIEW);
        this.movRelease = getColumnString(cursor, MOVIE_RELEASE);
        this.movPoster = getColumnString(cursor, MOVIE_POSTER);
        this.movRate = getColumnString(cursor, MOVIE_RATE);
        this.movRateCount = getColumnString(cursor, MOVIE_RATECOUNT);
    }

    private MovieItems(Parcel in) {
        this.id = in.readInt();
        this.movPoster = in.readString();
        this.movTitle = in.readString();
        this.movOverview = in.readString();
        this.movRelease = in.readString();
        this.movRateCount = in.readString();
        this.movRate = in.readString();
    }

    public static final Parcelable.Creator<MovieItems> CREATOR = new Parcelable.Creator<MovieItems>() {
        @Override
        public MovieItems createFromParcel(Parcel source) {
            return new MovieItems(source);
        }

        @Override
        public MovieItems[] newArray(int size) {
            return new MovieItems[size];
        }
    };
}
