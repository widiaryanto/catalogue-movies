package com.aryanto.widi.favoritemovies.databases;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {

    public static String TABLE_MOVIE = "movie";

    public static final class MovieColumns implements BaseColumns {
        public static String MOVIE_TITLE = "title";
        public static String MOVIE_OVERVIEW = "overview";
        public static String MOVIE_RELEASE = "release_date";
        public static String MOVIE_RATE = "vote_average";
        public static String MOVIE_RATECOUNT = "vote_count";
        public static String MOVIE_POSTER = "poster_path";
    }

    public static final String AUTHORITY = "com.aryanto.widi.cataloguemoviesuiux";
    public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
            .authority(AUTHORITY)
            .appendPath(TABLE_MOVIE)
            .build();

    public static String getColumnString(Cursor cursor, String columnName){
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public static int getColumnInt(Cursor cursor, String columnName){
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }
}
