package com.aryanto.widi.cataloguemoviesuiux.fragment;


import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aryanto.widi.cataloguemoviesuiux.R;
import com.aryanto.widi.cataloguemoviesuiux.activity.MainActivity;
import com.aryanto.widi.cataloguemoviesuiux.adapter.FavoriteAdapter;

import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.CONTENT_URI;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteFragment extends Fragment {

    private Cursor list;
    private FavoriteAdapter adapter;
    private RecyclerView rvCategory;
    private View view;

    public FavoriteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_now_playing, container, false);

        rvCategory = view.findViewById(R.id.rv_category);
        rvCategory.setLayoutManager(new LinearLayoutManager(getContext()));
        rvCategory.setHasFixedSize(true);

        adapter = new FavoriteAdapter(getContext());
        adapter.setListMovies(list);
        rvCategory.setAdapter(adapter);

        new LoadMovieAsync().execute();

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        new LoadMovieAsync().execute();
        ((MainActivity)getActivity()).setActionBar(R.string.favorite);
    }

    private class LoadMovieAsync extends AsyncTask<Void, Void, Cursor>{
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected Cursor doInBackground(Void... voids){
            return getContext().getContentResolver().query(CONTENT_URI, null, null, null, null);
        }

        @Override
        protected void onPostExecute(Cursor notes){
            super.onPostExecute(notes);
            list = notes;
            adapter.setListMovies(list);
            adapter.notifyDataSetChanged();

            if (list.getCount() == 0){
                showSnacbarMessage();
            }
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    private void showSnacbarMessage(){
        Snackbar.make(rvCategory, R.string.no_favorite, Snackbar.LENGTH_SHORT).show();
    }
}
