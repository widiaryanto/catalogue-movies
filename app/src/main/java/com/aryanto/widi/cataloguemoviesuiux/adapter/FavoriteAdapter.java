package com.aryanto.widi.cataloguemoviesuiux.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aryanto.widi.cataloguemoviesuiux.activity.DetailMovieActivity;
import com.aryanto.widi.cataloguemoviesuiux.R;
import com.aryanto.widi.cataloguemoviesuiux.custom.CustomOnItemClickListener;
import com.aryanto.widi.cataloguemoviesuiux.model.MovieItems;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.CONTENT_URI;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.MovieViewHolder> {

    private Cursor listMovies;
    private Context context;

    public FavoriteAdapter(Context context){
        this.context = context;
    }

    public void setListMovies(Cursor listMovies){
        this.listMovies = listMovies;
    }

    @Override
    public int getItemCount(){
        if (listMovies == null){
            return 0;
        }
        return listMovies.getCount();
    }

    private MovieItems getItem(int position){
        if (!listMovies.moveToPosition(position)){
            throw new IllegalStateException("Position Invalid");
        }
        return new MovieItems(listMovies);
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.now_playing_item, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position){
        final MovieItems movieItems = getItem(position);
        holder.textViewTitle.setText(movieItems.getMovTitle());
        holder.textViewOverview.setText(movieItems.getMovOverview());

        String dateRelease = parseDate(movieItems.getMovRelease());
        holder.textViewRelease.setText(dateRelease);

        Glide.with(context).load("http://image.tmdb.org/t/p/w500/"+movieItems.getMovPoster()).into(holder.imgPoster);

        holder.btnDetail.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallBack() {
            @Override
            public void onItemClicked(View view, int position) {
                MovieItems movieList = getItem(position);
                Intent intent = new Intent(context, DetailMovieActivity.class);
                intent.putExtra(DetailMovieActivity.EXTRA_TITLE, movieList.getMovTitle());
                intent.putExtra(DetailMovieActivity.EXTRA_POSTER, movieList.getMovPoster());
                intent.putExtra(DetailMovieActivity.EXTRA_OVERVIEW, movieList.getMovOverview());
                intent.putExtra(DetailMovieActivity.EXTRA_RELEASE, movieList.getMovRelease());
                intent.putExtra(DetailMovieActivity.EXTRA_RATE, movieList.getMovRate());
                intent.putExtra(DetailMovieActivity.EXTRA_RATE_COUNT, movieList.getMovRateCount());
                intent.putExtra("from", "FavoriteAdapter");
                intent.putExtra("model", movieItems);
                Uri uri = Uri.parse(CONTENT_URI+"/"+movieItems.getId());
                intent.setData(uri);
                context.startActivity(intent);
            }
        }));

        holder.btnShare.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallBack() {
            @Override
            public void onItemClicked(View view, int position) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, movieItems.getMovTitle()+" \n"+movieItems.getMovOverview());
                intent.setType("text/plain");
                context.startActivity(intent);
                Toast.makeText(context, "Share "+ movieItems.getMovTitle(), Toast.LENGTH_SHORT).show();
            }
        }));
    }

    private String parseDate(String tgl){
        String inputDate = "yyyy-MM-dd";
        String outputDate = "EEEE, MMM dd, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputDate, Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputDate, Locale.US);
        Date date;
        String string = null;

        try {
            date = inputFormat.parse(tgl);
            string = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return string;
    }

    class MovieViewHolder extends RecyclerView.ViewHolder{
        TextView textViewTitle, textViewOverview, textViewRelease;
        ImageView imgPoster;
        Button btnDetail, btnShare;

        MovieViewHolder(View itemView){
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.tv_item_name);
            textViewOverview = itemView.findViewById(R.id.tv_item_overview);
            textViewRelease = itemView.findViewById(R.id.tv_item_release);
            imgPoster = itemView.findViewById(R.id.img_item_photo);
            btnDetail = itemView.findViewById(R.id.btn_set_detail);
            btnShare = itemView.findViewById(R.id.btn_set_share);
        }
    }
}
