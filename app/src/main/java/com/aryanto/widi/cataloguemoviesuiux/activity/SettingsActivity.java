package com.aryanto.widi.cataloguemoviesuiux.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.provider.Settings;
import android.widget.Toast;

import com.aryanto.widi.cataloguemoviesuiux.AppCompatPreferenceActivity;
import com.aryanto.widi.cataloguemoviesuiux.R;
import com.aryanto.widi.cataloguemoviesuiux.scheduler.AlarmReceiver;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {

    private AlarmReceiver alarmReceiver = new AlarmReceiver();
    String dailyReminder, todayRelease, localeSetting;

    @Override
    public void onCreate(final Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_notification);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dailyReminder = getString(R.string.key_daily_reminder);
        todayRelease = getString(R.string.key_release_today);
        localeSetting = getString(R.string.key_settings_locale);

        findPreference(dailyReminder).setOnPreferenceChangeListener(this);
        findPreference(todayRelease).setOnPreferenceChangeListener(this);
        findPreference(localeSetting).setOnPreferenceClickListener(this);
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object o){
        String key = preference.getKey();
        boolean isOn = (boolean) o;

        if (key.equals(dailyReminder)){
            if (isOn){
                alarmReceiver.setRepeatingAlarm(getApplicationContext(), alarmReceiver.TYPE_REPEATING, "07:00", getString(R.string.content_daily_reminder));
            } else {
                alarmReceiver.cancelAlarm(getApplicationContext(), alarmReceiver.TYPE_REPEATING);
            }
            Toast.makeText(SettingsActivity.this, getString(R.string.label_daily_reminder)+ " " +(isOn ? getString(R.string.on) : getString(R.string.off)), Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (key.equals(todayRelease)){
            if (isOn){
                alarmReceiver.setRepeatingAlarm(getApplicationContext(), alarmReceiver.TYPE_RELEASE, "08:00", getString(R.string.content_release_today));
            } else {
                alarmReceiver.cancelAlarm(getApplicationContext(), alarmReceiver.TYPE_RELEASE);
            }
            Toast.makeText(SettingsActivity.this, getString(R.string.label_release_today)+ " " +(isOn ? getString(R.string.on) : getString(R.string.off)), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    @Override
    public boolean onPreferenceClick(Preference preference){
        String key = preference.getKey();

        if (key.equals(localeSetting)){
            Intent intent  = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(intent);
            finish();
            return true;
        }
        return false;
    }
}