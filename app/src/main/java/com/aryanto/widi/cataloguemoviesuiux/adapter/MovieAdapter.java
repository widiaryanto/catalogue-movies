package com.aryanto.widi.cataloguemoviesuiux.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aryanto.widi.cataloguemoviesuiux.R;
import com.aryanto.widi.cataloguemoviesuiux.model.MovieItems;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MovieAdapter extends BaseAdapter {

    private ArrayList<MovieItems> mData = new ArrayList<>();
    private LayoutInflater mInflater;
    private String data_overview;
    private Context context;

    public MovieAdapter(Context context){
        this.context = context;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(final MovieItems item){
        mData.add(item);
        notifyDataSetChanged();
    }

    public void setData(ArrayList<MovieItems> items){
        mData = items;
        notifyDataSetChanged();
    }

    public void clearData(){
        mData.clear();
    }

    @Override
    public int getCount() {
        if (mData == null)
            return 0;
        return mData.size();
    }

    @Override
    public MovieItems getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.movie_item, null);
            holder.imgPoster = (ImageView)convertView.findViewById(R.id.img_poster);
            holder.tvTitle = (TextView)convertView.findViewById(R.id.tv_title);
            holder.tvOverview = (TextView)convertView.findViewById(R.id.tv_overview);
            holder.tvRelease = (TextView)convertView.findViewById(R.id.tv_release);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.tvTitle.setText(mData.get(position).getMovTitle());

        String overview = mData.get(position).getMovOverview();
        if (TextUtils.isEmpty(overview)){
            data_overview = "No Overview";
        } else {
            data_overview = overview;
        }
        holder.tvOverview.setText(data_overview);

        String releaseDate = mData.get(position).getMovRelease();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = dateFormat.parse(releaseDate);
            SimpleDateFormat newDateFormat = new SimpleDateFormat("EEEE, MMM dd, yyyy");
            String dateRelease = newDateFormat.format(date);
            holder.tvRelease.setText(dateRelease);
        } catch (ParseException e){
            e.printStackTrace();
        }

        Glide.with(context).load("http://image.tmdb.org/t/p/w154/"+mData
                .get(position)
                .getMovPoster())
                .into(holder.imgPoster);
        return convertView;
    }

    private static class ViewHolder{
        ImageView imgPoster;
        TextView tvTitle;
        TextView tvOverview;
        TextView tvRelease;
    }
}