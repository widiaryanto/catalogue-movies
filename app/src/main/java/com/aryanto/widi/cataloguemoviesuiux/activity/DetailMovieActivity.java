package com.aryanto.widi.cataloguemoviesuiux.activity;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aryanto.widi.cataloguemoviesuiux.R;
import com.aryanto.widi.cataloguemoviesuiux.helper.MovieHelper;
import com.aryanto.widi.cataloguemoviesuiux.model.MovieItems;
import com.aryanto.widi.cataloguemoviesuiux.widget.StackWidget;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.CONTENT_URI;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_OVERVIEW;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_POSTER;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_RATE;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_RATECOUNT;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_RELEASE;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_TITLE;
import static com.aryanto.widi.cataloguemoviesuiux.widget.StackWidget.UPDATE_WIDGET;

public class DetailMovieActivity extends AppCompatActivity {

    public static String EXTRA_TITLE = "extra_title";
    public static String EXTRA_POSTER = "extra_poster";
    public static String EXTRA_RELEASE = "extra_release";
    public static String EXTRA_OVERVIEW = "extra_overview";
    public static String EXTRA_RATE_COUNT = "extra_rate_count";
    public static String EXTRA_RATE = "extra_rate";

    private TextView mvTitle, mvRelease, mvOverview, mvRatinng;
    private ImageView mvPoster;
    private MovieItems movieItems;
    private CheckBox favoriteBox;
    private MovieHelper movieHelper;
    private boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String from = getIntent().getStringExtra("from");

        mvTitle = (TextView)findViewById(R.id.mv_title);
        mvPoster = (ImageView)findViewById(R.id.mv_poster);
        mvRelease = (TextView)findViewById(R.id.mv_release);
        mvOverview = (TextView)findViewById(R.id.mv_overview);
        mvRatinng = (TextView)findViewById(R.id.mv_rating);
        favoriteBox = (CheckBox)findViewById(R.id.mv_favorite);

        String title = getIntent().getStringExtra(EXTRA_TITLE);
        String poster = getIntent().getStringExtra(EXTRA_POSTER);
        String release = getIntent().getStringExtra(EXTRA_RELEASE);
        String overview = getIntent().getStringExtra(EXTRA_OVERVIEW);
        String rate_count = getIntent().getStringExtra(EXTRA_RATE_COUNT);
        String rate = getIntent().getStringExtra(EXTRA_RATE);
        String dateRelease = parseDate(release);

        mvTitle.setText(title);
        mvOverview.setText(overview);
        mvRelease.setText(dateRelease);
        mvRatinng.setText(rate_count+" Ratings ("+rate+"/10)");
        Glide.with(getApplicationContext()).load("http://image.tmdb.org/t/p/w500/"+poster).into(mvPoster);

        favoriteBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (favoriteBox.isChecked()){
                    showAlertDialog(ALERT_DIALOG_ADD);
                } else if (!favoriteBox.isChecked()){
                    showAlertDialog(ALERT_DIALOG_DELETE);
                }
            }
        });

        if (from.equals("MovieAdapter")){
            movieItems = getIntent().getParcelableExtra("model");
        } else if (from.equals("FavoriteAdapter")){
            movieHelper = new MovieHelper(this);
            movieHelper.open();
            Uri uri = getIntent().getData();
            if (uri != null){
                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                if (cursor != null){
                    if (cursor.moveToFirst()){
                        movieItems = new MovieItems(cursor);
                        cursor.close();
                    }
                }
            }
            favoriteBox.setChecked(true);
        }

        if (favoriteBox.isChecked()){
            isEdit = true;
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if (movieHelper != null){
            movieHelper.close();
        }
    }

    final int ALERT_DIALOG_ADD = 10;
    final int ALERT_DIALOG_DELETE = 20;

    private void showAlertDialog(int type){
        final boolean isDialogAdd = type == ALERT_DIALOG_ADD;
        String dialogTitle, dialogMessage;

        if (isDialogAdd){
            dialogTitle = getResources().getString(R.string.dialog_title_add);
            dialogMessage = getResources().getString(R.string.dialog_message_add);
        } else {
            dialogTitle = getResources().getString(R.string.dialog_title_remove);
            dialogMessage = getResources().getString(R.string.dialog_message_remove);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setMessage(dialogMessage).setTitle(dialogTitle)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (isDialogAdd){
                            Toast.makeText(DetailMovieActivity.this, R.string.dialog_title_add, Toast.LENGTH_SHORT).show();
                            addFavorite();
                        } else {
                            getContentResolver().delete(getIntent().getData(), null, null);
                            Toast.makeText(DetailMovieActivity.this, R.string.dialog_title_remove, Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (isDialogAdd){
                            dialog.cancel();
                            favoriteBox.setChecked(false);
                        } else {
                            dialog.cancel();
                            favoriteBox.setChecked(true);
                        }
                    }
                })
                .show();
    }

    private String parseDate(String tgl){
        String inputDate = "yyyy-MM-dd";
        String outputDate = "EEEE, MMM dd, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputDate, Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputDate, Locale.US);
        Date date;
        String string = null;

        try {
            date = inputFormat.parse(tgl);
            string = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return string;
    }

    private void addFavorite(){
        String title = movieItems.getMovTitle();
        String overview = movieItems.getMovOverview();
        String release = movieItems.getMovRelease();
        String poster = movieItems.getMovPoster();
        String rate = movieItems.getMovRate();
        String rate_count = movieItems.getMovRateCount();

        ContentValues contentValues = new ContentValues();
        contentValues.put(MOVIE_TITLE, title);
        contentValues.put(MOVIE_OVERVIEW, overview);
        contentValues.put(MOVIE_RELEASE, release);
        contentValues.put(MOVIE_POSTER, poster);
        contentValues.put(MOVIE_RATE, rate);
        contentValues.put(MOVIE_RATECOUNT, rate_count);

        if (isEdit){
            getContentResolver().update(getIntent().getData(), contentValues, null, null);
        } else {
            getContentResolver().insert(CONTENT_URI, contentValues);
        }

        UpdateWidget();
    }

    public void UpdateWidget(){
        Intent i = new Intent(this, StackWidget.class);
        i.setAction(UPDATE_WIDGET);
        sendBroadcast(i);
    }
}