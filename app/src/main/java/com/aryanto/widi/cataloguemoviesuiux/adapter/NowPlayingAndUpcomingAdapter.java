package com.aryanto.widi.cataloguemoviesuiux.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aryanto.widi.cataloguemoviesuiux.activity.DetailMovieActivity;
import com.aryanto.widi.cataloguemoviesuiux.R;
import com.aryanto.widi.cataloguemoviesuiux.custom.CustomOnItemClickListener;
import com.aryanto.widi.cataloguemoviesuiux.model.MovieItems;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class NowPlayingAndUpcomingAdapter extends RecyclerView.Adapter<NowPlayingAndUpcomingAdapter.ViewHolder> {

    private ArrayList<MovieItems> movieLists;
    private Context context;

    private ArrayList<MovieItems> getMovieLists(){
        return movieLists;
    }

    public NowPlayingAndUpcomingAdapter(ArrayList<MovieItems> movieList, Context context){
        this.movieLists = movieList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.now_playing_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position){
        final MovieItems movList = movieLists.get(position);
        holder.title.setText(movList.getMovTitle());
        holder.overview.setText(movList.getMovOverview());

        String dateRelease = parseDate(movList.getMovRelease());
        holder.release.setText(dateRelease);

        Glide.with(context).load("http://image.tmdb.org/t/p/w500/"+movList
                .getMovPoster())
                .into(holder.poster);

        holder.btnDetail.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallBack(){
            @Override
            public void onItemClicked(View view, int position){
                MovieItems movieList = movieLists.get(position);
                Intent intent = new Intent(context, DetailMovieActivity.class);
                intent.putExtra(DetailMovieActivity.EXTRA_TITLE, movieList.getMovTitle());
                intent.putExtra(DetailMovieActivity.EXTRA_POSTER, movieList.getMovPoster());
                intent.putExtra(DetailMovieActivity.EXTRA_OVERVIEW, movieList.getMovOverview());
                intent.putExtra(DetailMovieActivity.EXTRA_RELEASE, movieList.getMovRelease());
                intent.putExtra(DetailMovieActivity.EXTRA_RATE, movieList.getMovRate());
                intent.putExtra(DetailMovieActivity.EXTRA_RATE_COUNT, movieList.getMovRateCount());
                intent.putExtra("from", "MovieAdapter");
                intent.putExtra("model", getMovieLists().get(position));
                context.startActivity(intent);
            }
        }));

        holder.btnShare.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallBack() {
            @Override
            public void onItemClicked(View view, int position) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, getMovieLists().get(position).getMovTitle()+" \n"+getMovieLists().get(position).getMovOverview());
                intent.setType("text/plain");
                context.startActivity(intent);
                Toast.makeText(context, "Share "+ getMovieLists().get(position).getMovTitle(), Toast.LENGTH_SHORT).show();
            }
        }));
    }

    private String parseDate(String tgl){
        String inputDate = "yyyy-MM-dd";
        String outputDate = "EEEE, MMM dd, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputDate, Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputDate, Locale.US);
        Date date;
        String string = null;

        try {
            date = inputFormat.parse(tgl);
            string = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return string;
    }

    @Override
    public int getItemCount(){
        return movieLists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView title, overview, release;
        ImageView poster;
        Button btnDetail, btnShare;

        ViewHolder(View itemView){
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.tv_item_name);
            poster = (ImageView)itemView.findViewById(R.id.img_item_photo);
            overview = (TextView)itemView.findViewById(R.id.tv_item_overview);
            release = (TextView)itemView.findViewById(R.id.tv_item_release);
            btnDetail = (Button)itemView.findViewById(R.id.btn_set_detail);
            btnShare = (Button)itemView.findViewById(R.id.btn_set_share);
        }
    }
}