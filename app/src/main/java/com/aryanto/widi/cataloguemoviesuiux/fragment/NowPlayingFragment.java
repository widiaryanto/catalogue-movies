package com.aryanto.widi.cataloguemoviesuiux.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aryanto.widi.cataloguemoviesuiux.BuildConfig;
import com.aryanto.widi.cataloguemoviesuiux.R;
import com.aryanto.widi.cataloguemoviesuiux.adapter.NowPlayingAndUpcomingAdapter;
import com.aryanto.widi.cataloguemoviesuiux.model.MovieItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NowPlayingFragment extends Fragment {

    private RecyclerView rvCategory;
    private RecyclerView.Adapter adapter;
    private ArrayList<MovieItems> movieLists;
    private View view;

    private static final String URL = BuildConfig.BASE_URL+"/now_playing?api_key="+BuildConfig.API_KEY+"&language=en-US";

    public NowPlayingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_now_playing, container, false);
        rvCategory = (RecyclerView)view.findViewById(R.id.rv_category);
        rvCategory.setHasFixedSize(true);
        rvCategory.setLayoutManager(new LinearLayoutManager(getActivity()));
        movieLists = new ArrayList<>();
        loadUrlData();
        return view;
    }

    private void loadUrlData(){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String response){
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray array = jsonObject.getJSONArray("results");
                    for (int i = 0; i < array.length(); i++){
                        JSONObject data = array.getJSONObject(i);
                        MovieItems movie = new MovieItems(data);
                        movie.setMovTitle(data.getString("title"));
                        movie.setMovPoster(data.getString("poster_path"));
                        movie.setMovOverview(data.getString("overview"));
                        movie.setMovRelease(data.getString("release_date"));
                        movie.setMovRate(data.getString("vote_average"));
                        movie.setMovRateCount(data.getString("vote_count"));
                        movieLists.add(movie);
                    }
                    adapter = new NowPlayingAndUpcomingAdapter(movieLists, getActivity());
                    rvCategory.setAdapter(adapter);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                Toast.makeText(getActivity(), "Error" + error.toString(), Toast.LENGTH_SHORT).show();
                loadUrlData();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }
}
