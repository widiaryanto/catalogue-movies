package com.aryanto.widi.cataloguemoviesuiux.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_OVERVIEW;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_POSTER;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_RATE;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_RATECOUNT;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_RELEASE;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.MovieColumns.MOVIE_TITLE;
import static com.aryanto.widi.cataloguemoviesuiux.databases.DatabaseContract.TABLE_MOVIE;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "dbmovie";
    private static final int DATABASE_VERSION = 1;

    private static String CREATE_TABLE_MOVIE = "create table "+TABLE_MOVIE+
            " ("+_ID+" integer primary key autoincrement, " +
            MOVIE_TITLE+" text not null, " +
            MOVIE_OVERVIEW+" text not null, " +
            MOVIE_POSTER+" text not null, " +
            MOVIE_RELEASE+" text not null, " +
            MOVIE_RATE+" text not null, " +
            MOVIE_RATECOUNT+" text not null);";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(CREATE_TABLE_MOVIE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1){
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_MOVIE);
        onCreate(db);
    }
}
