package com.aryanto.widi.cataloguemoviesuiux.activity;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.aryanto.widi.cataloguemoviesuiux.R;
import com.aryanto.widi.cataloguemoviesuiux.adapter.MovieAdapter;
import com.aryanto.widi.cataloguemoviesuiux.adapter.MovieAsyncTaskLoader;
import com.aryanto.widi.cataloguemoviesuiux.model.MovieItems;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<ArrayList<MovieItems>> {

    ListView listView;
    EditText edtMovie;
    Button btnCari;
    ImageView imgPoster;
    MovieAdapter adapter;

    static final String EXTRAS_MOVIE = "EXTRAS_MOVIE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().setTitle(getResources().getString(R.string.searching));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new MovieAdapter(this);
        adapter.notifyDataSetChanged();
        listView = (ListView) findViewById(R.id.lv_movie);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MovieItems item = (MovieItems) parent.getItemAtPosition(position);
                Intent intent = new Intent(SearchActivity.this, DetailMovieActivity.class);
                intent.putExtra(DetailMovieActivity.EXTRA_TITLE, item.getMovTitle());
                intent.putExtra(DetailMovieActivity.EXTRA_POSTER, item.getMovPoster());
                intent.putExtra(DetailMovieActivity.EXTRA_RELEASE, item.getMovRelease());
                intent.putExtra(DetailMovieActivity.EXTRA_OVERVIEW, item.getMovOverview());
                intent.putExtra(DetailMovieActivity.EXTRA_RATE_COUNT, item.getMovRateCount());
                intent.putExtra(DetailMovieActivity.EXTRA_RATE, item.getMovRate());
                intent.putExtra("from", "MovieAdapter");
                intent.putExtra("model", (MovieItems) parent.getItemAtPosition(position));

                startActivity(intent);
            }
        });

        edtMovie = (EditText) findViewById(R.id.edt_movie);
        Intent intent = getIntent();
        String query = intent.getStringExtra(EXTRAS_MOVIE);
        edtMovie.setText(query);

        imgPoster = (ImageView) findViewById(R.id.img_poster);
        btnCari = (Button) findViewById(R.id.btn_cari);
        btnCari.setOnClickListener(movieListener);
        String movTitle = edtMovie.getText().toString();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRAS_MOVIE, movTitle);
        getLoaderManager().initLoader(0, bundle, this);
    }

    @Override
    public Loader<ArrayList<MovieItems>> onCreateLoader(int id, Bundle args) {
        String movieTitle = "";
        if (args != null) {
            movieTitle = args.getString(EXTRAS_MOVIE);
        }
        return new MovieAsyncTaskLoader(this, movieTitle);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<MovieItems>> loader, ArrayList<MovieItems> data) {
        adapter.setData(data);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<MovieItems>> loader) {
        adapter.setData(null);
    }

    View.OnClickListener movieListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String titleMovies = edtMovie.getText().toString();
            if (TextUtils.isEmpty(titleMovies)) {
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString(EXTRAS_MOVIE, titleMovies);
            getLoaderManager().restartLoader(0, bundle, SearchActivity.this);
        }
    };
}
