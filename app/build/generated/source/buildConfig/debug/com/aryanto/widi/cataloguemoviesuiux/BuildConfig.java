/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.aryanto.widi.cataloguemoviesuiux;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.aryanto.widi.cataloguemoviesuiux";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from default config.
  public static final String API_KEY = "60d0c50bdc47444184bcd6f0acdfb12e";
  public static final String BASE_URL = "https://api.themoviedb.org/3/movie";
  public static final String BASE_URL_SEARCH = "https://api.themoviedb.org/3/search/movie";
}
